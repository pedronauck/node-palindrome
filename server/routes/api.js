const isPalindrome = require('../../utils/is-palindrome');

module.exports = (router) => {
  router.post('/is-palindrome', (req, res) => {
    const { text } = req.body;
    const status = isPalindrome(text) ? 200 : 400;

    res.status(status).send(`API Status: ${status}`);
  });

  return router;
};
