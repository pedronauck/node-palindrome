const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const apiRoutes = require('./routes/api');
const ejs = require('ejs');

const app = express();
const router = express.Router();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRoutes(router));
app.use('/', (req, res) => res.render('index'));

module.exports = app;
