Palindrome API
===

This is a simple api server that check if a string is palindrome or not.

First of all install Yarn via npm:

```bash
$ npm i -g yarn
```

So, you can install the dependencies:

```bash
$ yarn install
```

To run the server:

```bash
$ yarn start
```

To see the app running, open `http://localhost:3000`

# Test

You can run the tests suite running:

```bash
$ yarn test
```

# API

## `[GET] http://localhost:3000/api/is-palindrome`

Check is a string is palindrome.

| params | description | value |
| ------ | ----------- | ----- |
| text | Text to check | String |

### Response status

- `200` If the string is a palindrome
- `400` If the string isn't a palindrome
- `400` If the parameter `text` isn't a string
