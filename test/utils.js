const expect = require('chai').expect;
const isPalindrome = require('../utils/is-palindrome');

describe('utils', () => {
  describe('isPalindrome()', () => {
    it('should accept just a string', () => {
      expect(isPalindrome(0)).to.be.falsy;
    });

    it('should param exist', () => {
      expect(isPalindrome()).to.be.falsy;
    });

    it('should return if a str is palindrome', () => {
      expect(isPalindrome('ana')).to.be.true;
    });

    it('should trim special characters', () => {
      expect(isPalindrome('A man, a plan, a canal. Panama'));
    });
  });
})
