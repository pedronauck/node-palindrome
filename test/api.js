const expect = require('chai').expect;
const request = require('request');
const http = require('http');

const app = require('../server/app');

const PORT = process.env.PORT || 4000;
const API_URL = `http://localhost:${PORT}/api`;
const server = http.createServer(app);

app.set('port', PORT);

describe('api', () => {
  before(() => server.listen(PORT));
  after(() => server.close());

  describe('/is-palindrome', () => {
    it('should return 200 if is a palindrome', (done) => {
      request
        .post(`${API_URL}/is-palindrome`)
        .form({ text: 'A man, a plan, a canal. Panama' })
        .on('response', (res) => {
          expect(res.statusCode).to.equal(200);
          done();
        });
    });

    it('should return 400 if isn\'t a palindrome', (done) => {
      request
        .post(`${API_URL}/is-palindrome`)
        .form({ text: 'nope' })
        .on('response', (res) => {
          expect(res.statusCode).to.equal(400);
          done();
        });
    });
  });
});
