const isPalindrome = (str) => {
  if (!str || typeof str !== 'string') return false;

  const re = /[\W_]/g;
  const lowRegStr = str.toLowerCase().replace(re, '');
  const reverseStr = lowRegStr.split('').reverse().join('');

  return reverseStr === lowRegStr;
};

module.exports = isPalindrome;
