import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import App from './components/App';
import rootReducer from './ducks';

const logger = store => next => action => {
  if (typeof action !== 'function') {
    console.log('dispatching: ', action);
  }

  next(action);
};

const middleware = applyMiddleware(logger, thunk);
const store = createStore(rootReducer, middleware);

render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('root'));
