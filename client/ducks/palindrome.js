import fetch from 'isomorphic-fetch';

const API_URL = 'http://localhost:3000/api';

const IS_PALINDROME = 'IS_PALINDROME';
const TURN_CHECKED_FALSE = 'TURN_CHECKED_FALSE';
const CHANGE_INPUT_TEXT = 'CHANGE_INPUT_TEXT';

const initialState = {
  status: null,
  isChecked: false,
  text: ''
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case IS_PALINDROME:
      return { ...state, status: payload.status, isChecked: true };

    case TURN_CHECKED_FALSE:
      return { ...state, status: null, isChecked: false };

    case CHANGE_INPUT_TEXT:
      return { ...state, text: payload.text };

    default:
      return state;
  }
};

const isPalindrome = (payload) => ({
  type: IS_PALINDROME,
  payload
});

export const turnCheckedFalse = () => ({
  type: TURN_CHECKED_FALSE
});

export const changeInputText = (text) => ({
  type: CHANGE_INPUT_TEXT,
  payload: { text }
});

export const checkPalindrome = (text) => (dispatch) => {
  const opts = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ text })
  };

  fetch(`${API_URL}/is-palindrome`, opts)
    .then((res) => {
      dispatch(isPalindrome({ status: res.status }));
    });
};
