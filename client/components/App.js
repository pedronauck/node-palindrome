import React, { Component } from 'react';
import cn from 'classnames';
import fetch from 'isomorphic-fetch';
import { connect } from 'react-redux';

import * as palindromeActions from '../ducks/palindrome';

class App extends Component {
  handleChange = (ev) =>
    this.props.changeInputText(ev.target.value);

  handleCheckIfPalindrome = () =>
    this.props.checkPalindrome(this.props.text);

  componentWillReceiveProps(nextProps) {
    const { text, isChecked } = this.props;

    if (isChecked && (text !== nextProps.text)) {
      this.props.turnCheckedFalse();
    }
  }

  render() {
    const { isChecked, status, text } = this.props;

    const classes = {
      'is-palindrome': isChecked && status === 200,
      'isnot-palindrome': isChecked && status === 400
    };

    const inputClass = cn('input', classes);
    const msgClass = cn('result', classes)

    return (
      <div className="app">
        <h1 className="title">Type to check palindrome</h1>
        <div className="controller">
          <input className={inputClass} value={text} onChange={this.handleChange} />
          <button className="button" onClick={this.handleCheckIfPalindrome}>check</button>
        </div>
        {isChecked && (
          <p className={msgClass}>
            {status === 200 ? `✔︎ ${text} is palindrome` : `✘ ${text} isn't a palindrome`}
          </p>
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps, { ...palindromeActions })(App);
